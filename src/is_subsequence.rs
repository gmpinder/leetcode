pub fn run(s: String, t: String) -> bool {
    let mut s_iter = s.chars().into_iter();
    let mut curr_char = match s_iter.next() {
        Some(c) => c,
        None => return true,
    };

    for x in t.chars() {
        if x == curr_char {
            curr_char = if let Some(s) = s_iter.next() {
                s
            } else {
                return true;
            };
        }
    }
    false
}
