use clap::{Parser, Subcommand};

pub mod is_subsequence;
pub mod reverse_string;

#[derive(Parser, Debug)]
#[command(name = "Leetcode tester", author, version, about, long_about = None)]
pub struct LeetcodeArgs {
    #[command(subcommand)]
    pub command: Commands,
}

#[derive(Subcommand, Debug)]
pub enum Commands {
    ReverseString {
        #[arg()]
        string: String,
    },
    IsSubsequence {
        #[arg()]
        subsequence: String,

        #[arg()]
        string: String,
    },
    SortedSquares,
}
