use clap::Parser;
use leetcode::{is_subsequence, reverse_string, Commands, LeetcodeArgs};

fn main() {
    let args = LeetcodeArgs::parse();

    match args.command {
        Commands::ReverseString { string } => {
            let mut vec_str: Vec<char> = string.chars().collect();
            reverse_string::run(&mut vec_str);
            println!("{:?}", &vec_str);
        }
        Commands::IsSubsequence {
            subsequence,
            string,
        } => {
            println!("{}", is_subsequence::run(subsequence, string));
        }
    }
}
